import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Items } from '../model/Item';
import { ItemService } from '../service/item.service';

@Component({
  selector: 'app-guestdashboard',
  templateUrl: './guestdashboard.component.html',
  styleUrls: ['./guestdashboard.component.css']
})
export class GuestdashboardComponent implements OnInit {

  email:string | null=localStorage.getItem('email');
  item!:Items[];
  constructor(private userservice:ItemService,private router:Router) {
   }
  
  ngOnInit(): void {
    this.userservice.getAllItems().subscribe(data=>{this.item=data})
  }


}
